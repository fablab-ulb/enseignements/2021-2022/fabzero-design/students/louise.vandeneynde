## Bonjour à toi cher visiteur et bienvenue sur mon git!

| ![](images/avatar-photo.jpg) | Je m'appelle _Louise_, étudiante en MA2 et inscrite au module 3 du cours de design. Tu trouveras sur ce site un historique de mes travaux; sur ce bonne visite! |
| ------ | ------ |


## Parcours et anciens travaux

Après deux quadrimestres dans l'atelier Digital Fabrication Studio et un quadrimestre dans l'option architecture et design (module 1), j'ai développé un intérêt pour les nouvelles technologies ainsi que pour la matière. Un workshop sur la terre crue ainsi qu'un quadrimeste dans l'atelier Architecture Construite, à accentué cet intérêt. Pour en savoir plus sur mes anciens travaux n'hésite pas à jeter un oeil sur [mon ancien git](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/louise.vandeneynde/final-project/) ainsi que sur [mon portfolio](https://louisevdeynde.wixsite.com/my-site?fbclid=IwAR0wt7y_aN4J8U8ymwppig48rgBgUm5YnhevhqZg0hT5KTx6JY0oOIE0IX4)!



## Interface musée/enfants; premières idées...

| Média| Message |
| ------ | ------ |
| Maquette musée avec des miniatures des objets du musée* | ??? QUE COMMUNIQUER ??? |
| Evolution physique de la matière | Comment les objets en plastiques sont fabriqués? Travail sur la Plasticotek|

*Pour:
- contextualiser spatialement et historiquement
- Associer avec des couleurs, textures,...
- // avec le recyclage : imprimer les miniatures avec du plastique recyclé broyé avec un appareil de [Precious PLastic](https://preciousplastic.com)

