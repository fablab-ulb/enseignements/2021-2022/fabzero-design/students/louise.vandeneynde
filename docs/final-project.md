# Projet final

## Premières idées
L’idée de base du projet est de faire des miniatures de certains objets du musée pour que les enfants puissent les manipuler. Après qu’en faire? Il faut un objectif pédagogique! Les associer à des couleurs ? Des textures ?  Les mettre ds un contexte ? Sur une ligne du temps ? ...

Néanmoins les miniatures font écho à l'univers du jouet et donc au jeu.
Quel jeu ?  Monopoli ? Jeu de l’oie ? Cluedo ? ... ?

La première idée de jeu sur laquelle nous sommes parti est le jeu des 7 familles. Les enfants y joueraient après la visite. Des cartes qui représentent graphiquement les différentes caractéristiques des objets/des plastiques devront être associées entre elles et avec la miniature correspondante. 

![](images/V1.png)

Peut-être serait-il possible de complexifier cela en faisant une sorte de cluedo en mode « retrouver l’objet volé au musée » (au lieu du meurtrier ds le jeu principal) vu qu’il y a plusieurs salles; on pourrait aussi utiliser les designers... 


Pour l'instant, une liste des objets choisi a été réalisée (un objet "iconique" de chaque salle/podium et donc de chaque plastique), certains ont d'ailleurs été imprimés ou modelisés. Enfin un référencement de leurs caracteristiques a aussi ete fait. Tout cela est consultable sur notre [drive partagé](https://drive.google.com/drive/folders/1IC8eZ5aImHiJgpSsAIYqE-zpqKESjecy?usp=sharing). 


## Suite du projet

Nous avons d'abord finalisé le choix des objets du musée.

| Endless Flow | Dirk Vander Kooij  |
| ------ | ------ |
| ![](images/ENDLESS_FLOW.png) | Podium "recyclés" - RECYCLE.   Caractéristiques : Recyclé - Frigo - Impression 3D |

| Chica | Jonathan de Pas, Donato d'Urbino, Paolo Lomazzi, Giorgio de Curso   |
| ------ | ------ |
| ![](images/CHICA.jpg) | Première salle - ABS.   Caractéristiques : Modulaire - Empillable - Pour enfants |

| Pratone | Pietro Derossi, Giorgio Ceretti et Riccardo Rosso   |
| ------ | ------ |
| ![](images/PRATONE.png) | Podium "radical design" - PUR.   Caractéristiques : Mou - Gigantisme - Détournement |

| Dondolo | Dirk Vander Kooij  |
| ------ | ------ |
| ![](images/DONDOLO.Jpeg) | Salle “conquête spatiale” – GRP (fibre de verre).   Caractéristiques : Futuriste/espace - Chaise à bascule - Anti-gravité |

| Cantilever | Verner Pantone  |
| ------ | ------ |
| ![](images/CANTILEVER.jpg) | Podium “porte à faux".   Caractéristiques : Prouesse technique - resistant - 2 pieds (porte-à-faux) |

| Translation | Alain Gilles  |
| ------ | ------ |
| ![](images/TRANSLATION.png) | Podium "recyclés" - RECYCLE.   Caractéristiques : Recyclé - Composé de plein de petits objets - Multicolore |


Nous avons ensuite essayé de repenser les cartes afin de les rendre plus accessibles aux enfants en y ajoutant des couleurs. 

![](images/V2.png)

Pour finir nous sommes reparti sur des cartes utilisant plus des pictogrammes afin d'être plus compréhensibles.

![](images/V3.png)

Finalement, nous avons changé quelques picto car nous n'avions pas les images en pgn.

![](images/V4.png)


Afin de complexifier le jeu, nous avons décidé d'ajouter d'autres cartes (histoire et bonus) ainsi qu'un plateau de jeu et des pions à l'effigie de designers célèbres. 
![](images/Histoire.png)  ![](images/Bonus.png)  ![](images/Plateau.png)  ![](images/Pions.jpg)


Nous avons aussi créé une boite de "recompenses" avec des granules de plastique imprimés en 3D à une échelle plus importante et un double fond qui cache une récompense...

![](images/granules.png) 

Voici le scénario et les règles du jeu que nous allons essayer cette semaine avec les enfants :

![](images/regles.jpg)

## Visite des enfants au Fablab 02/12

Aujourd'hui les enfants sont venu tester nos prototypes "sales" et ils ont plutôt apprécié notre jeu.

 ![](images/visite_enfants.png) 


Cela nous a permis de nous rendre compte de ce qui allait, ce qui marchait et au contraire ce qui n'allait pas dans notre jeu. 

Ce qui a marché:
- Le jeu en lui-même a bien fonctionné : l'idée « d'être dans le musée » dans le jeu
- Nombre de cases du jeu et de cartes idéal
- Enfants curieux, se souvenaient de la visite et ont appris de nouvelles choses
- L'entraide, l'esprit d'équipe recherché -> TOP
- Système de récompenses (granulés) les poussent à se dépasser ou réfléchir davantage

A améliorer : 
- Imprimer les objets dans leur vraie couleur (comme dans le musée)
- Afficher le nom à côté l'objet
- d'avantage de cartes vertes (caractéristiques)
- Changement sur les règles : carte verte 1 point, bleue 2, rouge 3
- Boîtes de rangement et design du plateau (3 a1 de bois pour le plateau, améliorer les cartes et les pions 3D -> + résistants)
- Prix: objets miniatures de poche à donner à la fin du jeu
- Retravailler le plateau de jeu -> + design et complexe


Ce que nous devons améliorer maintenant c'est le plateau en lui même et confectionner un packaging. 


## Suite du projet: 

Plateau
Pour le plateau pour éviter des soucis ds les pliages, nous avons décidé de fonctionné sous forme de puzzel. 

![](images/Nouvelle_idee_de_pateau.png)

Après avoir acheter du carton gris 2, nous l'avons découper à la laser et nous avons réimprimé le plan sur un papier de plus grande qualité. 

![](images/Decoupe_laser_puzz.jpeg)
![](images/Plan_satine.jpeg)


- Cartes

Des modificartions ont été faites sur certaines questions, en donnant quelques conseils aux enfants pour trouver la bonne réponse. Nous avons également additionné certaines des cartes vertes, car nous avons constaté qu'elles n'étaient pas suffisantes la dernière fois. Celles-ci ont été réimprimées dans un papier plus rigide.

- Miniatures

Pour les miniatures, nous avons dû en réimprimer certaines pour les avoir aux couleurs d'origine des objets du musée, rendant le jeu le plus proche possible de la réalité. Malheureusement, il reste encore une chaise à imprimer dans sa vraie couleur, la Cantilever. L'impression a échoué plusieurs fois donc il va falloir la redémarrer... Pour les miniatures : nous avons aussi commencé à imprimer des mini versions de nos miniatures.
Ce sont les prix que nous remettrons à chaque groupe d'enfants qui jouent au jeu ! Voici quelques-unes des mini miniatures qui sont prêtes :

- Pions

Pour les pions nous avons décidé de les imprimer également en 3D pour une meilleure rigidité et qu'ils soient visuellementplus attractif.
Ils ont été modélisé sur Fusion 360 et imprimer sur machine 3D.
Voici le rendu des pions :

- Boîte de jeu

Un beau jeu passe aussi par son enveloppe, si la boite est belle et actrative, le jeu gagne en crédibilité.
La boîte est en bois, découpée à la scie cicrulaire sur table et mesure 50x40x15 dans le but d'y ranger le plateau, les pions et toutes les miniatures. 

![](images/Boite.jpg)

Cette boîte est évidement temporaire car c'est un prototype aux bonnes dimensions. Pour le jury final, elle comportera les cotés gravés et décorés, un intérieur noir (soit peint soit velour agrafé pour le toucher moins rugueux) et l'extérieur poncé et vernis.


## Préjury 16/12

Plateau: 
- Efficace et lisible mm si ça pourrait l’être + 
- Le voir cô une découverte (utilisée comme manette pédagogique)  ou redécouverte du musée
- NB: besoin d’être le + reproductible possible

MS:
- on ne se rend pas compte que c’est le plan du musée
- Pas assez parlant, besoin d’être + vivant, d’avoir + d’expression, + graphique et ludique


- Ajout contexte ? 
- Ajouter relief ex estrades ? // jeu labyrinthe
- Encoches ? 
- Matérialité ? 
- Cmt le monter ? Vrai puzz avec volume et intégration (attention où on coupe!)
- Photo de l’objet au lieu du nom ?  
- Même échelle que miniature ? 
- Peut prendre + de place ?



Cartes: 
- Revoir caractéristiques (parfois pas adaptées ou vont pr +sieurs objets); cohérence!
- Attention au termes employés; parfois mal choisis, et aux concepts parfois difficiles à comprendre 
- Picto pas même registres ; besoin homogénéité 
- Pas ≠ niv de difficulté ms juste ≠ types de questions

—> Parallèle entre design cartes et plateau !!


Pions:
- Pas stables 
- Besoin d’être + travaillés (+ d’infos sur designer) 


Granulés: 
- Ok sur le principe ms besoin d’être + parlants (on ne se rend pas compte de ce que c’est); 3D ? 
- Boite aussi en 3D ? 


Prix:
- Pas collectif, chacun en voudra 1 —> + petits genre autocollant ? 
- Recipiant qui se rempli pr pouvoir gagner



Boite:
- A adapter aux dimensions


Règles du jeu:
- Attention à la cadence


![](images/prejury.jpeg)


## Du préjury au jury final...
Après les remarques du préjury nous avons essayé d'ameliorer les points clé de notre jeu qui faisaient défaut:


- Le plateau: 
Premièrement, nous avons aussi décidé de modifier le plateau quelque peu graphiquement en ajoutant y les couleurs et la typographique du musée ce qui le personnalise d'autant plus. Nous avons rajouté des petits encarts d'informations complémentaires afin de mieux visualiser comment les objets fonctionnent. 

![](images/Plateau_final_autocad.png)

Nous avons aussi arrondi les bords des podiums et des du parcours afin pour le rendre plus esthétique, plus "fini" et ajouté des podiums en 3D surmontés d'une plaque de plexi gravé afin de donner plus de volume. 

![](images/podium.jpg) ![](images/plexis.jpg)

Enfin, nous avons décidé de ne plus utiliser le système de puzzel qui n'était pas très esthétique dans le collage des impressions sur le carton. Nous sommes donc parti sur une impression sur bache pour sa facitité son rangement ainsi que sa mise en oeuvre lors de l'installation du jeu. De plus, sa texture imperméable permettra de le nettoyer facilement et donc de le conserver longtemps.

![](images/Plateau_final_imprime.jpeg)

- La boite: 
Le plateau ayant été radicalement changé, nous avons recommencer la boite. Nous y avons intégré des séparation de rangement mais aussi des finitions tel que du verni et une poignée. 

![](images/boite.jpeg)

- Les cartes :
Au niveau des cartes nous avons retravaillé les cartes vertes pour avoir une uniformité grapique au niveau des pictogrammes qui tous du logiciel powerpoint. 

![](images/Cartes_vertes.png)
![](images/Cartes_vertes_suite.png)

- Les pions : 
Pour les pions nous avons décidé de changer leur base car ils n'étaient pas assez stables. Nous avons aussi rajouté des informations à l'arrière pour completer.

![](images/Cartes_pions_a_imprime.png) ![](images/base.jpeg)
![](images/pions_V.jpeg)
![](images/Pions_R.jpeg)

- Les "granulés": 
Nous avons décidé d'abandonner l'idée des granulés, un peu grossier et pas assez reconnaissables, pour des jetons de récompenses plus "classiques" avec une gravure de nos objets dessus.

![](images/jeton.jpeg)

Nous avons aussi retravaillé la boite qui les contient.

![](images/Boite_jetons.jpeg)


- Les "prix": 
Afin d'avoir non plus une récompense de groupe mais une pour chaque joueur, nous avons décidé de remplacer les mini-miniatures de base par des autocollants à l'effigies des objets; un chouette souvenir ludique pour les enfants.

![](images/recompense.jpg)


- Les règles du jeu:
Enfin nous avons réécrit les règles du jeu en les mettant bien en page comme un facicule.

![](images/Regles1.png)
![](images/Regles2.png)
![](images/Regles_final.jpeg)

Voici le résulat final!

![](images/FINALLL.jpeg)
