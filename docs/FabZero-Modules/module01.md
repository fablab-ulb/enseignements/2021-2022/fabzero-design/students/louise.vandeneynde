# Semaine 1

En ce début d'année scolaire 2021, avec les étudiants du module 3, nous travaillons avec le musée du design de Bruxelles afin de créer une interface à destination des enfants. Situé à deux pas de l'Atomium, le musée abrite, depuis 2015, une collection de plus de 1000 d'objets en plastique datant de 1950 jusqu'à nos jours ; mais comment impliquer les enfants, que leur communiquer et comment le faire ? Voici quelques questions auxquelles nous tenterons de répondre lors de ce quadrimestre

## 1. Plasticotek et Lab

Nous avons tout d'abord rencontré deux responsables du musée, Christina et Terry, avec qui nous travaillerons cette année. Ils nous ont directement fait entrer dans le vif du sujet en nous parlant .... de plastique ! La suite de ce module est un mélange de notes prises pendant les différentes présentations auxquelles nous avons assisté ainsi que d'informations récoltées par ci par là sur internet afin de combler les connaissances notamment via ces deux vidéos sur le sujet : [Scilabus - D'où vient le nombril des bouteilles ?](https://www.youtube.com/watch?v=LqNnnsQxAFg) et [C'est pas sorcier - Le plastique ça nous emballe](https://www.youtube.com/watch?v=irFEnEZhlNM).


Les plastiques sont regroupés en **3** grandes familles: 


| Thermoplastiques | Thermodurcissable| Elastomère |
| ------ | ------ | ------ |
| Regroupes les plastiques réversibles : une fois chauffés ils prennent la forme du moule, durcissent en refroidissant et, si on les réchauffe, on peut recommencer le procédé pour en faire une autre pièce.   | Le plastique est chauffé et prend la forme du moule. Cela signifie que ce dernier doit être chauffé uniformément à une température élevée. Sa fabrication coûte très cher ce qui explique la production de masse de l'objet ainsi que sa production rapide afin de rentabiliser les investissements. | Regroupe tout ce qui est gomme, caoutchouc, ... |


Les **5 principaux procédés** pour créer un objet en plastique sont par :

| Injection |  Extrusion | Soufflage | Rotomoulage | Thermoformage |
| ------ | ------ | ------ | ------ | ------ |
| Des billes de plastiques sont réchauffées puis poussées par une vis sans fin dans un moule. Un point d'injection (endroit où l'on a poussé le plastique) est visible une fois l'objet terminé. Peu esthétique, certains designers veulent à tout prix le cacher. L'objet final est plein.  | Cette technique est la même que celle utilisée pour les profilés → on pousse le plastique dans un moule puis on le découpe.  | Cette technique est la même que celle utilisée pour les profilés → on pousse le plastique dans un moule puis on le découpe.	Cette technique est presque identique aux deux précédentes sauf qu'au lieu de pousser le plastique, ce dernier est soufflé. L'objet final est vide et a des différences d'épaisseur.	 | Une fois le moule chauffé on y verse des billes de plastiques et la machine se met en mouvement. L'objet final est souvent de taille importante et est vide à l'intérieur. Cette méthode donne un aspect rugueux au plastique et est souvent utilisée avec le PE ou encore le PP. | Une feuille de plastique est chauffé entre deux plaques puis un moule donne la forme voulue à l'objet final. Diverses difficultés sont à noter : l'épaisseur (très fin et léger devient très fragile), la forme, ...)


## 2. Identification du plastique
Nous nous sommes ensuite intéressés à l'identification d'un objet en plastique. Grâce à plusieurs boites d'échantillons, un questionnaire et des photos sur le [site de l'institut de Gand](https://www.designmuseumgent.be/fr/collection/projet/plastiques), on peut identifier le plastique qui constitue un objet. On demande alors à quel type de plastique on a à faire (mousse, film, rigide, élastomère) s'il vient de l'industrie, d'un artiste ou si c'est un échantillon, son année, s'il a un logo, un symbole, ... Plusieurs tests faisant appel aux sens sont aussi effectués sur l'objet : visuel, tactile, olfactif, ... Tout cela a pour but de savoir comment conserver ou restaurer l'objet ; cela dépend évidemment de ce que l'on veut qu'il représente. Pour une pièce que l'on veut exposer c'est l'aspect esthétique qui prime, on cherche donc à restaurer l'objet par exemple en le polissant pour effacer les traces d'usure (cela ne peut être fait que si le plastique est teinté dans la masse). Au contraire, on peut vouloir que l'objet soit une trace du passé, là on essaie alors d'arrêter sa dégradation.

## 3. La réserve 
Finalement, nous avons eu la chance de visiter la réserve et même de manipuler les différents objets une fois munis de gants en plastiques. Ces derniers sont rangés dans des étagères par famille et isolé de leur support par une mousse afin d'éviter qu'ils ne collent ou décolorent. Les conditions de conservations sont donc très importantes ; la température par exemple ne doit pas être trop basse sinon les "gommes" se casseraient ni pas trop élevée sinon les "gonflables" se dégradent.


![](images/Carte.jpg)


| ABS | PVC | PMMA | PP | PE | PS |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Apparus dans les années 50/60, les objets fabriqués en ABS sont principalement obtenus soi par injection soit par extrusion. Rigides et d'aspect brillant, ils peuvent être colorés, mais lorsqu'ils vieillissent ils se décolorent, jaunissent et deviennent cassants.| 	Ces plastiques souples sont utilisés pour fabriquer des objets gonflables dont certaines pièces sont soudées entre elles. Leur revêtement peut avoir des aspects variés : translucide, opaque, mat, brillant, coloré, ... Mais avec le temps ce dernier commence à "transpirer" le rendant visqueux et collant.  | 	Plus communément appelés plexiglas, ces objets sont produits en série et transparents même si leur qualité optique n'est pas exceptionnelle. | Polypropylène  | Polyéthylène  | Polystyrène  |

![](images/M1.Reserve1.png)



| GRP | Impression 3D | Résine, caoutchou, bakelite, melamine | PC | PUR |
| ------ | ------ | ------ | ------ | ------ |
| Désigne les objets réalisés en fibre de verre que l'on voit d'ailleurs encore sur l'objet fini ce qui le rend facilement reconnaissable. Ils sont des légers et résistants. | IMP | Confère un aspect noble à un petit objet, mais a un certain poids | Polycarbonate sont très résistants, d'aspects brillants et ont une très bonne qualité optique. | Regroupe les objets fabriqués en mousse. La densité de cette dernière donnera un objet plus ou moins rigide pouvant aller jusqu'à une structure autoportante. Plus ou moins molle, cette matière s'abîme facilement, elle se fissure, la poussière vient s'y déposer ce qui rend la surface difficile à nettoyer sans l'altère. |

![](images/M1.Reserve2.png)


