# Semaine 3

## Retour sur les dossiers pédagogiques
Après avoir jeté un coup d'œil aux quatre dossiers pédagogiques qui nous ont été envoyé, nous avons discuté de ce qu'on en pense. Il s'est avéré que le plus complet des 4 est **Plastic-plastoc**. En effet, ce dernier donne autant des informations sur le lieu en lui-même que sur les objets (même si sur ce point il pourrait aller plus en détail) contrairement aux autres qui donnent trop d'informations, qui sont trop long, ...

## Petite parenthèse pour stimuler la créativité : les stratégies obliques
_"Fais quelque chose d'ennuyeux"_

## Dossier pédagogique 
Permet au professeur de préparer sa visite, de bien transmettre, échanger avec un groupe. Cela lui montre certaines choses et l'aide. C'est un objet qui va transmettre quelque chose.

**VECTEUR → INFORMATION**

Mais pourquoi choisir ce musée assez récent ? 
Créer des contenus et/ou objets médiateurs aiderait à faire ce choix. En effet, des savoirs avant, pendant et après la visite peuvent être utilisées pour des activités. En outre, une préparation en amont est nécessaire tel que le visionnage de vidéos éducatives telles que celle présentées dans le git de la semaine 1 : [Scilabus - D'où vient le nombril des bouteilles ?](https://www.youtube.com/watch?v=LqNnnsQxAFg) et [C'est pas sorcier - Le plastique ça nous emballe](https://www.youtube.com/watch?v=irFEnEZhlNM). En effet l'acquisition d'une partie de la connaissance avant la visite permet de ne plus la transmettre pendant la visite. 

Mais en fait, qu'est-ce que doit apprendre l'élève ? 
Raison pour laquelle l'objet existe ? En plus d'avoir des socles de compétences différents entre les élèves, il faut rester dans le programme. De plus, le musée va évoluer et donc les médias qui vont y être utilisés le doivent aussi. Le contexte, l'évolution et la scénarisation vont impacter l'objet que l'on va faire.  
Quand on teste des objets avant de les commercialiser on fait appel à des groupes cibles : les séniors (accessible) et les militaires (fonctionnels).

## Visite 
Cette visite présentée par Christina est un complément à celle de Terry de la semaine précédente et s'attarde plus sur le côté technique des objets.

**Introduction**

| Chaise Universale | Joe Colombo |
| ------ | ------ |
| ![](images/M3.Colombo.jpg) | Réalisée en ABS injecté en une pièce, cette chaise a été dessinée pour pouvoir être empilable ce qui facilite notamment son transport en grand nombre. En effet, dû au coût élevé du moule, la pièce doit être standardisée, répliquée en masse et donc transportée et emballée de la manière la plus optimale et simple possible. Certains détails ne sont pas esthétiques, mais sont dû au moule par exemple les rayures, le trou, ... La première édition de cette chaise était modulable : on pouvait changer les pieds afin de changer sa hauteur (chaise pour enfant, de bureau, de bar). Cela répondait à une nécessité due au contexte d'une époque où les gens voulaient se meubler pour pas cher tout en ayant des objets personnalisés dans un espace limité.|

**Salle 1**

| Modularité |  |
| ------ | ------ |
| ![](images/M3.Modularite.jpg) | La modularité et personnalisation des objets : on a une base, constituée d'un module qui peut être répliqué et agencé/empilé différemment, à laquelle on ajoute des accessoires. Cela permet de changer la taille de l'objet et donc sa fonction. Plusieurs gammes sont donc créées chacune avec son nom propre.  |


Cette modularité est un impératif pour les designers : le dessin doit tenir compte de l'utilisateur, car l'objet doit être vendu. L'utilisateur fait partie du procédé de fabrication. On faisait des études de comportement pour pouvoir adapter les objets. Avant, c'étaient les architectes qui dessinaient les objets, mais depuis les années 50/60 le designer est devenu une profession. 


| Valentine | Ettore Sottsass |
| ------ | ------ |
| ![](images/M3.Valentine.jpg) | Réalisé en ABS coloré, le design de cette machine à écrire est total : il prend en compte l'objet, son packaging qui la protège en l'encastrant et sa communication.
Son invention va de pair avec son époque, fin des années 60 en Italie notamment, où l'on a de plus en plus de temps libre et où le faible coût des objets en plastique rend leur achat possible. Il y a un changement de paradigme, on veut bouger et on sort des stéréotypes particulièrement de celui de la secrétaire n'est plus à son bureau, mais qui écrit où elle veut.   |


**Salle 2**

| Pantone |  |
| ------ | ------ |
| ![](images/M3.porteafaux.jpg) | Pantone: toute une histoire derrière... V1: quantité limitée ; re-travail trop lourde, mais doit qd même tenir. V2: nouveau matériau et donc propopositions → nervures = détails structurels. V3: la forme change, plus continue et organique et le poids aussi. On n'a plus besoin des propriétés mécaniques conférées par les nervures. V4: même matériau, mais la hauteur de l'assise change (c'est une autre époque où les gens sont plus grands) et de nouvelles couleurs apparaissent : bleu, mauve, noir au lieu des traditionnels orange, jaune et rouge. L'idée est et était de créer une chaise la plus légère et la moins épaisse possible en un seul moule, donc en une seule pièce. Cela a été rendu possible notamment grâce aux matériaux de plus en plus résistants. La forme a changé légèrement au fil des ans, mais surtout son poids. |

On y retrouve aussi des pièces de design anthropomorphes dont la réalisation est artisanale et créée pour un événement

**Salle 3**

Dédiée à l'impact qu'a eu la conquête spatiale sur le design, cette salle va être retravaillée pour montrer la démonstration de l'investissement et de l'innovation dans le plastique notamment en créant des espaces totaux.

| Dondolo |  |
| ------ | ------ |
| ![](images/M3.Dondolo_.jpg) | Véritable prouesse technologique, la dondolo est le résultat d'une extrusion sur côté possible, car réalisée en fibre de verre renforcée. Mais quid des nervures ? |


| Capapé transparent |  |
| ------ | ------ |
| ![](images/M3.Canapetransparent.jpg) |  Son moule pesait 30T, l'injection s'est faite par les pieds. La pièce elle-même bien que visuellement légère est très lourde. |


| Radical Design |  |
| ------ | ------ |
| ![](images/M3.Radical.jpg) |  Fin des années 60/70, un mouvement de protestation contre le capitalisme nait et des objets en plastiques sont alors réalisés en édition limitée. Le cactus par exemple ses bras a été ajoutés et la finition, les détails, réalisée à la main comme le vernis qui confère un revêtement brillant. Le processus est plus artisanal, réalisé par un designer et non l'industrie. |


| Travail sur la résine | Pratt |
| ------ | ------ |
| ![](images/M3.Resine.jpg) |   Il a réalisé diverses expériences pour essayer de comprendre comment exploiter le matériau notamment en laissant couler la résine. En modifiant la quantité de polyuréthane, la chaise se solidifiait de plus en plus. Malgré tout, la texture finale reste assez étrange au toucher : un peu molle et colle un peu. Ses pièces sont uniques, car il demandait aux ouvriers de choisir le mélange des couleurs ; il faisait de l'anti-design. |





