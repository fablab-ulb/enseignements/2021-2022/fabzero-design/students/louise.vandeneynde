# Semaine 2

Comment faire revenir le public au musée ? 

Ex: 
- Safari Smile BX/StemLab Tokyo : musées instagrammables, appropriation réseaux sociaux
- Andenne musée céramique -> atelier céramique
- custom fourchette frite puis impression 3D
- custom couverture livre puis découpe/gravure

Il faut améliorer l'espace et proposer des outils de médiations afin d mieux le comprendre notamment pour les enfants. 

Pour cela, l'option va s'organiser un peu comme le [Museomix](https://www.museomix.org/) (pendant trois jours consécutifs on doit créer un prototype sur base d'une thématique mais) mais sur une période beaucoup plus longue. On va devoir produire une réflexion sur les outils numériques comme outils de médiation. 

Première idée : utiliser des miniatures sous forme de jeu plateau VS 3D physique.

Mais avant tout il faut bien poser la question : qu'est-ce qui est important de montrer du musée aux enfants ?

- Regarder l'œuvre VS changer le regarde sur l'œuvre
- Quid du designer ? 
-Pourquoi faire une nouvelle chaise ?
- Que retenir ?  "Donner à manger ou une canne à pêche ?" 
- Déconstruction de l'objet : comprendre comment il est construit permet de reconstruire quelque chose d'autre

+ Dossier pédagogique à l'intension des enseignants pour qu'ils préparent leur visite. Il contient des pistes de réflexion, des sélections du musée, ...

## Réflexions et brainstorming
Quelques idées lancées à la mer... 
- assemblage pièces pour créer un objet design
- plasticine : moule pour créer des objets du musée
- objets qui parlent de comment ils ont été fait : animation des objets/toy's story
- jeu de piste avec boites sensorielles et informations
- audio-guide
- évolution de la matière : chips - mousse - gonflable

## Visite 

Nous avons ensuite eu le plaisir d'avoir une visite guide du musée (collection permanente). Les prochaines notes sont reprises de notes prises lors de la visite et quelquefois complété par des infos venant d'internet. 

Pour commencer, un peu d'histoire...

En 2014, l'Atomium achète la collection de Philippe Decelle. Cette dernière n'est composée que d'objets en plastique qu'il a récupéré jusqu'en 2010. Trois événements ont influencé le commencement de cette collection : 
- en tant que plasticien mr Decelle est un amoureux du PMMA
- aquisition d'un fauteil: le _Garden Egg_ de Peter Ghyczy
- récupération d'une chaise de Joe Colombo _Universale_ qui été à jeter (à l'époque on commençait à jeter le plastique)
Avant d'être exposées au musée du design de Bruxelles (avant ADAM) en 2015 (ouverture du musée), la collection a d'abord été exposée au _Plasticarium_ puis à l'exposition _"Orange Dreams"_, ensuite à celle de la _"Fondation pour l'Architecture"_ (appelé aujourd'hui C.I.V.A.). En cédant sa collection à une seule entité, Philippe Decelle a permis de ne pas disperser le patrimoine Belge. L'exposition permanente est donc basée sur cette collection et permet de montrer l'impact du plastique dans le design, mais aussi dans la vie quotidienne.  


### Salle 1
La première salle met l'accent sur le rêve américain et les racines de la production industrielle.

Suite à la crise de 29, il y a une revalorisation de l'environnement américain qui se crée. Les designers vont se pencher sur le problème et inventer de nouveaux types d'objets dont ceux en plastique ; ils vont venir produire les objets. 

Après la 2e guerre mondiale, on assiste à une diffusion massive du "made in USA" en Europe grâce notamment au plan Marshall ; leurs objets vont s'immiscer dans nos intérieurs.

Raymond Loewy qui est l'un des 1ers designer connu et reconnu (il a aussi créé des logos de marques très connus) à tel point qu'il a fait la couverture du Time (journal qui véhicule des idées fortes) ou encore Eames, Nealson,... 

Puis les échanges entre designer étrangers, notamment italiens, va se développer et s'exporter, au même titre que le design américain, dans le monde entier.

Au même moment, une nouvelle philosophie se développe particulièrement grâce aux progrès technologiques : la société de consommation et de production.
- le plastique n'est pas cher, mais le moule si du coup, on produit en masse
- nouvelle esthétique : le plastique permet de cacher la technique dans des boitiers
- Le matériau est très modulable 
- Cela permet de diminuer la taille de l'objet

Mais cela répond aussi à une volonté d'émancipation des carcans familiaux, de la tradition : on ne veut plus de gros meubles lourds et massifs, mais des choses mobiles. Cela est en parallèle avec la société de loisir qui se développe. Le plastique, grâce à sa légèreté permet cette volonté de mouvement. // Pop 

En Europe, Kartel, une industrie italienne, va participer à diffuser le mobilier en plastique et donc à sa production et consommation. 

Cette première salle met donc en avant cette production de masse engendrée par la société de consommation et de loisir. Pour l'illustrer, chaque objet dans cette vitrine est portable. 

Ce genre de mobilier est dit éphémère, car facilement remplaçable. Aujourd'hui il a une valeur historique et est assez onéreux comparé au prix que l'on avait avant en produisant en très grande quantité en tout cas jusqu'à la crise pétrolière...

La matière est très malléable et des pigments peuvent être ajoutés ; c'est surtout de l'ABS.


### Salle 2
La deuxième salle à pour but de montrer la plasticité du plastiqué autrement dit : ce qu'il est possible de faire avec ce matériau.
Sur le premier podium par exemple, on peut voir du mobilier en forme de dent de Wandell Castel. Sur celui du centre accueille le travail de Gunter Beltigz qui joue sur les formes de la nature. Sa chaise par exemple est réduite à ce que l'on a vraiment besoin et est pensée pour être placée aussi bien à l'intérieur qu'à l'extérieur grâce à ses graces rainures prévues pour… avec l'image de la femme objet fortement véhiculée à l'époque. On est entre le design, la sculpture et l'art.
Enfin, on arrive à une thématique plus technique qui est celle du porte-à-faux. Le podium montre l'ensemble des chaises en porte-à-faux de la collection placées les unes après les autres pour montrer l'évolution du procédé technique : comment créer une chaise sans pied arrière et où la tension est mise sur le devant de l'assise sans que cela casse ? Parfois réalisée en un seul moule.

### Salle 3
Toujours dans les années 60, la 3ᵉ salle permet de se rendre compte de l'impact de la science-fiction et de la conquête spatiale sur les designers: comment ils ont travaillé les formes par rapport à ce qu'il se passait historiquement.
Pantone: création d'univers totaux, tout est fait en plastique et agencé avec une certaine logique.
La dondolo par exemple est l'une des œuvres les plus marquantes. Son aspect futuriste fait penser que si on s'assied dessus, c'est comme si on était en apesanteur. 
Les deux autres objets de ce podium on des formes très intéressantes : comme si c'était deux feuilles de plastique que l'on aurait plié.
Calka: même veine scifi ; anecdote sur le bureau du président à l'Élysée: chaque président français demande à un designer français de refaire son bureau. Celui ci est une réplique de celui de Pompidou, on peut imaginer le contraste avec le lieu.

Une nouvelle caractéristique vient s'ajouter à la matière: la transparence qui rend visuellement l'objet plus léger. On n'hésite pas à y ajouter des pigments.

Toujours dans l'idée de voyager léger, une autre caractéristique séduisante rendue possible grâce aux avancées technologiques est la possibilité de gonfler des objets créés à partir de feuilles de PVC soudées entre elles. Cela rend l'objet facilement transportable et accentue encore plus son côté éphémère que l'on peut jeter après usage. Quasar Khan a produit toute une gamme de mobilier gonflable.

Mais tout cela ne va pas sans certaines contestations contre cette société de consommation et de production de masse...
Un nouveau courant va donc se développer : l'architecture/le design radical ; plutôt italien. Ces artistes vont s'amuser avec la matière et créeront beaucoup d'objets en mousse (polyuréthane) et à l'aspect particulier... Ce ne va pas être de grande production pour lutter contre la pollution qu'engendre le plastique. 

Mr Decelle n'a pas que collectionné du design en plastique, mais aussi de l'art. Dans ce "cabinet de curiosité" les œuvres sont mises en lien avec les objets. Cela permet de montrer comment les plastiques vont être utilisés par les artistes et comment ils vont entrer partout ; dans tous les domaines grâce notamment à d'énormes avancées technologiques (ex :nylon)

Partie résine

Puis on arrive dans une partie plus chronologique. Après les années 60/70 et le design radical, on a eu le postmodernisme qui veut revenir à un lien entre les objets et leurs utilisateurs. Ex : Zonini, Starck, ...
Il y a deux courants : le menfist sotssae et kigme ? 
On passe alors des couleurs flash de la Pop aux pastels plus sages, on revisite des archétypes (ex :trône), ...

Sur le podium en face on voit comment les grandes entreprises comme Kartel se sont internationalisées grâce notamment à leur collaboration avec des designers connus pour renouveler leur image de marque ex : Starck et Kartel, cheval à bascule bendo ? Son esthétisme est très minimaliste et il a une forme de poutre en métal courbe. Internationalisation des maisons d'éditions et nouvelle esthétique.

Dans les années 90, un nouveau courant influencé en partie par l'art minimal va se créer : le minimalisme. C'est une philosophie de vie : qui se traduit en termes d'objets. Ces derniers sont plus fins, plus esthétiques, plus en harmonie avec leur environnement. Il n'y a ni ornement ni fioriture.

Avec le développement des nouvelles technologies, ex : ordi, fait avec de nouvel matériau →  lien minimalisme. La technique n'est plus cachée, mais visible ; elle fait partie intégrante de l'objet et tient place d'ornement. 

Impression 3D, aspect noble, Starck moulé en une pièce

Question du recyclage apparait pour faire face à la production de masse du boum du plastique et surtout de la crise pétrolière et la pollution qui est créée. Plastique recyclé VS usage unique. On reproduit des objets/du mobilier avec des objets cassés. Ex : chaise Coca, mais greenwashing + forme de pub détournée 

Nouvel usage en termes de design : on retourne à l'artisanat, on recherche plus la technique, la relation avec une communauté, le travail avec des matériaux et outils sur place. On est sur une vision bien différente du design.
