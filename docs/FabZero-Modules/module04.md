# Semaine 4

Cette semaine nous avons accueilli Yan, le professeur de la classe qui viendra visiter le musée la semaine prochaine, et Steph et Flo au SteamLab. Ensemble avec Terry nous avons discuter de comment on allait travailler avec les enfants et nous leur avons aussi fait une petite visite guidée du musée. Ensuite nous avons réfléchi à comment adapter cette viste à des enfants: en réduisant et en ciblant certains objets qui pourraient les intéresser.


![](images/Design_18_objets.png)
